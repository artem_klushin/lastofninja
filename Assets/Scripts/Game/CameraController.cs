﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    /// <summary>
    /// Скорость передвижения
    /// </summary>
    public float Duration = 1.0F;

    /// <summary>
    /// Следовать за игроком
    /// </summary>
    public bool FollowPlayer
    {
        set
        {
            _cameraFollowScript.enabled = value;
        }
    }

    // Позиция камеры по умолчанию
    private Vector3 _defaultPos;
    // Начальная позиция движения
    private Vector3 _startPos;
    // Конечная позиция движения
    private Vector3 _endPos;

    // Размер камеры по умолчанию
    private float _defaultSize;
    // Начальный размер камеры
    private float _startSize;
    // Конечный размер камеры
    private float _endSize;

    // Время начала движения
    //private float _startTime;

    // Флаг: нужно перемещать камеру
    private bool _moveToTarget = false;

    // Флаг: движется ли камера к цели
    private bool _isMoveToTarget = false;

    // Камера
    private Camera _selfCamera;

    // Скрипт слежения за игроком
    private CameraFollow _cameraFollowScript;

    //Переменная для SmoothDamp CameraSize
    private float cameraSizeSmooth=1.0f;

    //Переменная для SmoothDamp CameraPosition
    private Vector3 cameraPositionSmooth;

    //Переменная для SmoothdampCamSize
    float camSizeChek;

    //Переменная для SmoothdampCamPos
    Vector3 camPosChek;

    // Скорость перемещения
    //private float _speed;

    void Awake()
    {
        _selfCamera = GetComponent<Camera>();

        _cameraFollowScript = GetComponent<CameraFollow>();
        if (_cameraFollowScript == null)
            Debug.LogException(new System.Exception("CameraFollow script didn't find in " + transform.name));
        else
            _cameraFollowScript.enabled = false;

        
        //_speed = 1f / Duration;
    }
    void Start()
    {
        _defaultPos = transform.position;
        _defaultSize = _selfCamera.orthographicSize; 
    }

    /// <summary>
    /// Призумить камеру к цели
    /// </summary>
    /// <param name="target">Позиция цели</param>
    /// <param name="needCameraSize">Конечный размер камеры</param>
    public void MoveTo(Vector3 target, float needCameraSize)
    {
        _isMoveToTarget = true;
        _startPos = transform.position;
        _endPos = new Vector3(target.x, target.y, -15f);
        //Debug.Log(_endPos);

        _startSize = GetComponent<Camera>().orthographicSize;
        _endSize = needCameraSize;

        //_startTime = Time.time; НЕ ИСПОЛЬЗУЕТСЯ
        _moveToTarget = true;
    }

    /// <summary>
    /// Отзумит камеру в исходное положение
    /// </summary>
    public void Reset()
    {
        _isMoveToTarget = false;
        _startPos = transform.position;
        _endPos = _defaultPos;

        //_startSize = new Vector2(GetComponent<Camera>().orthographicSize, 0);
        _startSize = GetComponent<Camera>().orthographicSize;
        _endSize = _defaultSize;

        //_startTime = Time.time;  НЕ ИСПОЛЬЗУЕТСЯ
        _moveToTarget = true;
    }

    void Update()
    {
        if (_moveToTarget)
        {

            // Начало изменения положения камеры
            transform.position = new Vector3 (
            Mathf.SmoothDamp(transform.position.x, _endPos.x, ref cameraPositionSmooth.x, 0.15f),
            Mathf.SmoothDamp(transform.position.y, _endPos.y, ref cameraPositionSmooth.y, 0.15f),
            Mathf.SmoothDamp(transform.position.z, _endPos.z, ref cameraPositionSmooth.z, 0.15f)
            );

            if (_startPos.x > _endPos.x)
            {
                camPosChek.x = _startPos.x - _endPos.x;
            }

            if (_startPos.y > _endPos.y)
            {
                camPosChek.y = _startPos.y - _endPos.y;
            }

            if (_startPos.x < _endPos.x)
            {
                camPosChek.x = _endPos.x - _startPos.x;
            }

            if (_startPos.y < _endPos.y)
            {
                camPosChek.y = _endPos.y - _startPos.y;
            }


            if (camPosChek.x <= 0.05f && camPosChek.y <= 0.05f)
            {
                _selfCamera.orthographicSize = _endSize;
            }
            // End


            // Начало изменения размера камеры
            _selfCamera.orthographicSize = Mathf.SmoothDamp(_selfCamera.orthographicSize, _endSize, ref cameraSizeSmooth, 0.15f);

            if (_selfCamera.orthographicSize > _endSize) { 
                camSizeChek = _selfCamera.orthographicSize - _endSize;
                }

            if (_selfCamera.orthographicSize < _endSize){
                camSizeChek = _endSize - _selfCamera.orthographicSize;
                }


            if (camSizeChek <= 0.05)
            {
                _selfCamera.orthographicSize = _endSize;
            }
            // End

            //Вот это я не понял что такое
            if (transform.position == _endPos || _selfCamera.orthographicSize == _endSize)
            {
                _moveToTarget = false;
                if (_isMoveToTarget)
                    OnZoomed();
                else
                    OnUnzoomed();
            }
        }
    }

    /// <summary>
    /// Приближение
    /// </summary>
    public event System.EventHandler<System.EventArgs> Zoomed;
    private void OnZoomed()
    {
        System.EventHandler<System.EventArgs> handler = Zoomed;
        if (handler != null)
            handler(this, new System.EventArgs());
    }

    /// <summary>
    /// Удаление
    /// </summary>
    public event System.EventHandler<System.EventArgs> Unzoomed;
    private void OnUnzoomed()
    {
        System.EventHandler<System.EventArgs> handler = Unzoomed;
        if (handler != null)
            handler(this, new System.EventArgs());
    }

}
