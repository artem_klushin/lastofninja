﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    /// <summary>
    /// Дистанция по оси X на которую
    /// игрок может уйти без камеры
    /// </summary>
    public float xMargin = 1f;

    /// <summary>
    /// Дистанция по оси Y на которую
    /// игрок может уйти без камеры
    /// </summary>
    public float yMargin = 1f;

    /// <summary>
    /// Как плавно камера будет
    /// двигаться за целью по оси Х 
    /// </summary>
    public float xSmooth = 8f;

    /// <summary>
    /// Как плавно камера будет
    /// двигаться за целью по оси У
    /// </summary>
    public float ySmooth = 8f;

    /// <summary>
    /// Максимальная кордината по оси Х и У
    /// куда может передвинуться камера
    /// </summary>
    public Vector2 maxXAndY;

    /// <summary>
    /// Минимальная кордината по оси Х и У
    /// куда может передвинуться камера
    /// </summary>
    public Vector2 minXAndY;

    // transform игрока
    private Transform _playerTransform;


    void Awake()
    {
    }

    void OnEnable()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    //------------------------//
    // Проверка по краю оси Х //
    //------------------------//
    private bool CheckXMargin()
    {
        // true если дистанция между камерой и игроком по оси Х больше, чем xMargin
        return Mathf.Abs(transform.position.x - _playerTransform.position.x) > xMargin;
    }

    //------------------------//
    // Проверка по краю оси У //
    //------------------------//
    private bool CheckYMargin()
    {
        // true если дистанция между камерой и игроком по оси Y больше, чем yMargin
        return Mathf.Abs(transform.position.y - _playerTransform.position.y) > yMargin;
    }


    void FixedUpdate()
    {
        // Изначально координаты цели это текущие координаты камеры
        float targetX = transform.position.x;
        float targetY = transform.position.y;

        // Если игрок за xMargin...
        if (CheckXMargin())
            // ... координта Х цели должна скоректироваться между координатой Х камеры и текущей координатой Х игрока
            targetX = Mathf.Lerp(transform.position.x, _playerTransform.position.x, xSmooth * Time.deltaTime);

        // Если игрок за уMargin...
        if (CheckYMargin())
            // ... координта У цели должна скоректироваться между координатой У камеры и текущей координатой У игрока
            targetY = Mathf.Lerp(transform.position.y, _playerTransform.position.y, ySmooth * Time.deltaTime);

        // Координаты цели не могут быть болше максимума и меньше минимума
        targetX = Mathf.Clamp(targetX, minXAndY.x, maxXAndY.x);
        targetY = Mathf.Clamp(targetY, minXAndY.y, maxXAndY.y);

        // Установка позиции для камеры
        transform.position = new Vector3(targetX, targetY, transform.position.z);
    }
}
