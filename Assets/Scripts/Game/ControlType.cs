﻿using UnityEngine;
using System.Collections;

public class ControlType : MonoBehaviour
{
    public bool LeftMoveButton;
    public bool RightMoveButton;
    public bool JumpButton;
    public LayerMask touchInputMask;

    private Camera _interfaceCamera;

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void Start()
    {
        _interfaceCamera = GetComponent<Camera>();
    }

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void Update()
    {
        RightMoveButton = false;
        LeftMoveButton = false;
        JumpButton = false;

#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            
            Touch[] myTouches = Input.touches;

            for (int i = 0; i < myTouches.Length; ++i)
            {

                string collidername = "0";

                Touch touch = myTouches[i];
                Ray ray = _interfaceCamera.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, 0));
                RaycastHit hit;
                
                if (Physics.Raycast(ray, out hit))
                {
                    
                        if (hit.collider != null)
                        {
                            collidername = hit.collider.gameObject.name;
                        }

                        if (collidername == "UI-ButtonMoveLeft")
                        {
                        LeftMoveButton = true;
                    }

                        if (collidername == "UI-ButtonMoveRight")
                        {
                        RightMoveButton = true;
                    }
                        

                        if (collidername == "UI-ButtonJump")
                        {
                        JumpButton = true;
                    }



                } // if (Physics.Raycast(ray, out hit))
                
                
            }
            
        }
       
#endif

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    } // Update


} // Main
