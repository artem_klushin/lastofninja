﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class GameController : MonoBehaviour
{
    private CameraController _cameraController;
    private Camera _camera;

    private Level[] _levels;

    private Level _currentLevel;

    private PackageInfo _info;
    public GameObject controlButtons;

    void Start()
    {
        Transform tmp;
        controlButtons = GameObject.Find("ControlButtons");
        tmp = GameObject.Find("GameCamera").transform;
        if (tmp == null)
            Debug.LogException(new System.Exception("GameCamera didn't find in " + transform.name));
        else
        {
            _camera = tmp.GetComponent<Camera>();
            if (_camera == null)
                Debug.LogException(new System.Exception("Camera component didn't find in " + transform.name));

            _cameraController = tmp.GetComponent<CameraController>();
            if (_cameraController == null)
                Debug.LogException(new System.Exception("CameraController script didn't find in " + transform.name));
            else
            {
                _cameraController.Zoomed += _cameraController_Zoomed;
                _cameraController.Unzoomed += _cameraController_Unzoomed;
            }
        }

        _info = PackageManager.Instance.CurrentPackageInfo;

        _levels = new Level[4];

        for (int i = 0; i < _levels.Length; i++)
        {
            tmp = transform.FindChild("Level-0" + (i+1));
            if (tmp != null)
            {
                _levels[i] = tmp.GetComponent<Level>();
                _levels[i].Info = _info.Levels[0];
            }
            else
                Debug.LogException(new System.Exception("Can't find object Level-0" + (i + 1)));
        }

        Interface.Instance.StartGame += Interface_StartGame;
        Interface.Instance.RestartGame += Interface_RestartGame;
        Interface.Instance.NextLevel += Interface_NextLevel;

        TimeLine.Instance.TimeOver += TimeLine_TimeOver;

        Interface.Instance.SetState(Interface.STATE.SELECT_LEVEL);

        TimeLine.Instance.SetValue(_info.Time);
    }

    void OnDestroy()
    {
        _cameraController.Zoomed -= _cameraController_Zoomed;
        _cameraController.Unzoomed -= _cameraController_Unzoomed;

        Interface.Instance.StartGame -= Interface_StartGame;
        Interface.Instance.RestartGame -= Interface_RestartGame;
        Interface.Instance.NextLevel -= Interface_NextLevel;

        TimeLine.Instance.TimeOver -= TimeLine_TimeOver;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0) && (_info.State == PACKAGE_STATE.AVAIBLE || _info.State == PACKAGE_STATE.COMPLITE))
            PickLevel();

        if (Input.GetKeyUp(KeyCode.B))
        {
            Interface.Instance.SetState(Interface.STATE.NONE);
            _currentLevel.Disable();
            _cameraController.Reset();
        }
    }

    private void PickLevel()
    {
        RaycastHit2D hit = Physics2D.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), Vector2.down);
        if (hit && hit.transform.name.Contains("Level"))
        {
            Interface.Instance.SetState(Interface.STATE.NONE);
            if (_currentLevel != null)
            {
                _currentLevel.Finish -= _currentLevel_Finish;
                _currentLevel.Died -= _currentLevel_Died;
            }
            _currentLevel = hit.transform.GetComponent<Level>();
            _currentLevel.Finish += _currentLevel_Finish;
            _currentLevel.Died += _currentLevel_Died;
            Vector3 point = _currentLevel.CameraPoint.position;
            float size = _currentLevel.CameraSize;
            _cameraController.MoveTo(point, size);
        }
    }

    private void UpdateLevelInPackage()
    {
        int isPackComplite = 0;

        for (int i = 0; i < _info.Levels.Length; i++)
        {
            if (_info.Levels[i].Number == _currentLevel.Info.Number)
                _info.Levels[i] = _currentLevel.Info;

            if (_info.Levels[i].IsComplite)
                isPackComplite++;
        }

        if (isPackComplite == _info.Levels.Length)
            _info.State = PACKAGE_STATE.COMPLITE;

        PackageManager.Instance.CurrentPackageInfo = _info;
    }

    #region Event's handlers

    private void _cameraController_Zoomed(object sender, System.EventArgs e)
    {
        Interface.Instance.SetState(Interface.STATE.TAP_TO_START_GAME);
        _currentLevel.Enable();
    }

    private void _cameraController_Unzoomed(object sender, System.EventArgs e)
    {
        _currentLevel.Disable();
        Interface.Instance.SetState(Interface.STATE.SELECT_LEVEL);
    }

    private void Interface_RestartGame(object sender, System.EventArgs e)
    {
        TimeLine.Instance.StartTimer();
        _currentLevel.Run();
        //Вызываются кнопки управления персонажем
        controlButtons.GetComponent<Animator>().SetBool("On",true);
    }

    private void Interface_StartGame(object sender, System.EventArgs e)
    {
        TimeLine.Instance.Resume();
        _currentLevel.Run();
        //Вызываются кнопки управления персонажем
        controlButtons.GetComponent<Animator>().SetBool("On", true);
    }

    private void Interface_NextLevel(object sender, System.EventArgs e)
    {
        Interface.Instance.SetState(Interface.STATE.NONE);
        _cameraController.Reset();
    }

    private void TimeLine_TimeOver(object sender, System.EventArgs e)
    {
        throw new System.NotImplementedException();
    }

    private void _currentLevel_Died(object sender, System.EventArgs e)
    {
        Interface.Instance.SetState(Interface.STATE.TAP_TO_RESTART);
        //Скрываются кнопки управления персонажем
        controlButtons.GetComponent<Animator>().SetBool("On", false);
    }

    private void _currentLevel_Finish(object sender, System.EventArgs e)
    {
        Interface.Instance.SetState(Interface.STATE.TAP_TO_NEXT_LEVEL);
        TimeLine.Instance.Stop();
        //Скрываются кнопки управления персонажем
        controlButtons.GetComponent<Animator>().SetBool("On", false);
    }

    #endregion
}
