﻿using UnityEngine;
using System.Collections.Generic;

public class Mine : MonoBehaviour
{
    /// <summary>
    /// Сила взрыва
    /// </summary>
    public float ExplosionForce = 100.0f;

    /// <summary>
    /// Востонавливаемая мина или нет
    /// </summary>
    public bool IsRegenerable = false;

    /// <summary>
    /// Вкючена ли мина сразу
    /// </summary>
    public bool Enabled;

    // Радиус взрыва
    //private float _explosionRadius; - НЕ ИСПОЛЬЗУЕТСЯ

    // Список ближайших коллайдеров
    private List<Collider2D> _nearsColliders = new List<Collider2D>();

    // Система частиц
    private ParticleSystem _boomParticles;

    // Аниматор
    private Animator _animator;

    // Анимации
    private int _disabledAnim = Animator.StringToHash("Off");
    private int _enabledAnim = Animator.StringToHash("On");
    private int _boomAnim = Animator.StringToHash("Boom");
 

    // Кллайдер
    private Collider2D _collider;

    //Таймер для назначения силы
    private float _localTimer=1;
    private bool _timerON = false;

    void Awake()
    {
        
        Transform tmp;

        // _explosionRadius = GetComponent<CircleCollider2D>().radius;  НЕ ИСПОЛЬЗУЕТСЯ

        tmp = transform.Find("Boom-FX");
        if (tmp != null)
            _boomParticles = tmp.GetComponent<ParticleSystem>();

        _animator = GetComponentInChildren<Animator>();
        if (_animator == null)
            Debug.LogException(new System.Exception("Can't get Animator component in Mine"));

        Collider2D[] colliders = GetComponents<Collider2D>();

        foreach (Collider2D item in colliders)
        {
            if (!item.isTrigger)
                _collider = item;
        }
    }

    void Update()
    {
        // Если мина не востанавливаемая, то после взрыва выключить ее
        AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
        if (stateInfo.shortNameHash == _disabledAnim && !IsRegenerable)
            _animator.gameObject.SetActive(false);

        if (_timerON)
        {
            TimerGO();
        }
    }

    /// <summary>
    /// Возвращение к изначальному состоянию
    /// </summary>
    private void TimerGO()
    {
        _localTimer = _localTimer - 1;

    }
    public void Reset()
    {
        _timerON = false;
         _localTimer = 1;
        _animator.gameObject.SetActive(true);
        //Чистим список коллайдеров.
        _nearsColliders.Clear();

        if (Enabled) { 
            _animator.Play(_enabledAnim);
            _collider.enabled = true;
        }
        if (!Enabled)
        {
            _animator.Play(_disabledAnim);
            _collider.enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // Если появились части RiggidBody и триггер их засек
        if (other.gameObject.tag == "PlayerRagDoll")
        {
            _timerON = true;
            //Находим все части RagDoll
            _nearsColliders.Add(other);
            
            //Придаем ускорение найденным частям RagDoll. (Вызываем AddExplosionForce)
            foreach (Collider2D item in _nearsColliders)
            {
                if (_localTimer>0)
                {
                AddExplosionForce(item.GetComponent<Rigidbody2D>());
                }
            }
            _nearsColliders.Clear();
            
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);

        // Если выключен
        if (stateInfo.shortNameHash == _disabledAnim)
        {
            // Если востонавливаемый, то востановить
            if (IsRegenerable && other.gameObject.tag == "Player")
            {
                _animator.Play(_enabledAnim);
                _collider.enabled = true;
            }

            // инчае ничего не делать
            else
            {
                return;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        
        AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);

        // Если выключен, то ничего не делать
        if (stateInfo.shortNameHash == _disabledAnim)
            return;


        // Если появились части RiggidBody, то взрывать Explode
        if (coll.gameObject.tag == "Player" || coll.gameObject.tag == "PlayerRagDoll")
        {
            Explode();  
        }
    }

    /// <summary>
    /// Придание силы физ. объекту
    /// </summary>
    /// <param name="body">Физ. объект</param>
    private void AddExplosionForce(Rigidbody2D body)
    {
        float _localExplosionForce = Random.Range(ExplosionForce/2, ExplosionForce);
        //var dir = (transform.position - body.transform.position);
        //float wearoff = 1 - (dir.magnitude / 1.0f);
        body.velocity = Vector2.zero;
        float x = Random.Range(-1.0F, 1.0F);
        float y = Random.Range(-1.0F, 1.0F);
        body.AddForce(new Vector2 (x * _localExplosionForce, y * _localExplosionForce), ForceMode2D.Impulse);
    }

    /// <summary>
    /// Взрыв
    /// </summary>
    private void Explode()
    {   

        _collider.enabled = false;
        _animator.Play(_boomAnim);
        _boomParticles.Play();
        


    }
}
