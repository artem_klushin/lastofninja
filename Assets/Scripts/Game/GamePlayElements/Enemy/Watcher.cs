﻿using UnityEngine;
using System.Collections;

public class Watcher : MonoBehaviour
{
    public float MoveSpeed = 0.5f;

    private Collider2D _leftGroundChecker;
    private Collider2D _rightGroundChecker;

    private Transform _leftWallChecker;
    private Transform _rightWallChecker;

    private Transform _sensor;

    private Animator _animator;

    private enum STATE
    {
        FIND,
        MOVE
    }

    private STATE _state = STATE.FIND;

    private Vector2 _moveDirection;

    void Awake()
    {
        _leftWallChecker = transform.Find("LeftWallChecker");
        _rightWallChecker = transform.Find("RightWallChecker");
        _sensor = transform.Find("Sensor");

        Collider2D[] colliders = GetComponents<Collider2D>();

        // Поиск сенсоров
        foreach (Collider2D item in colliders)
        {
            if (item.isTrigger)
            {
                if (item.offset.x > 0)
                    _rightGroundChecker = item;
                else
                    _leftGroundChecker = item;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_state == STATE.FIND)
        {
            // Проверка вражины
            if (PlayerControl.Instance == null)
                return;

            RaycastHit2D hit = Physics2D.Linecast(transform.position, PlayerControl.Instance.transform.position, 1 << LayerMask.NameToLayer("Player"));
            Debug.DrawLine(transform.position, PlayerControl.Instance.transform.position, Color.red);
            if (hit && hit.transform.gameObject.layer != LayerMask.NameToLayer("Ground"))
            {
                if (PlayerControl.Instance.transform.position.x > transform.position.x)
                    _moveDirection = Vector2.right;
                else
                    _moveDirection = Vector2.left;
                _state = STATE.MOVE;
            }
        }
        else if (_state == STATE.MOVE)
        {
            transform.Translate(_moveDirection * MoveSpeed);

            if (_moveDirection == Vector2.right)
            {
                Collider2D coll = Physics2D.OverlapPoint(_rightWallChecker.position);
                if (coll && coll.gameObject.layer == LayerMask.NameToLayer("Ground"))
                    _state = STATE.FIND;
            }
            else
            {
                Collider2D coll = Physics2D.OverlapPoint(_leftWallChecker.position);
                if (coll && coll.gameObject.layer == LayerMask.NameToLayer("Ground"))
                    _state = STATE.FIND;
            }

            // Проверка стены вправо
            /*if (_moveDirection == Vector2.left)
            {
                if (Physics2D.Linecast(transform.position, _leftWallChecker.position, 1 << LayerMask.NameToLayer("Ground")))
                    _state = STATE.FIND;
            }
            else if (_moveDirection == Vector2.right)
            {
                if (Physics2D.Linecast(transform.position, _rightWallChecker.position, 1 << LayerMask.NameToLayer("Ground")))
                    _state = STATE.FIND;
            }*/
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {
        _state = STATE.FIND;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
    }
}
