﻿using UnityEngine;
using System.Collections;

public class FinishDoor : MonoBehaviour
{
    private Collider2D _collider;
    private TriggerEnabler _enabler;

    public void Awake()
    {
        _collider = GetComponent<Collider2D>();
        _enabler = GetComponent<TriggerEnabler>();
    }

    void Update()
    {
        _collider.enabled = _enabler.IsTriggered;
    }
}
