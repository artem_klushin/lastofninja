﻿using UnityEngine;
using System.Collections;

public class Spring : MonoBehaviour
{
    /// <summary>
    /// Сила пружины
    /// </summary>
    public float Power = 50f;
    public Vector2 currentSpeed;
    

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
            currentSpeed = other.GetComponent<Rigidbody2D>().velocity;
           
            if (currentSpeed.y < 0)
            {
            currentSpeed.y = currentSpeed.y * -1.0f;
            }
            other.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, Power + currentSpeed.y), ForceMode2D.Impulse);
    }
}
