﻿using UnityEngine;
using System.Collections;

public class TimeCoin : MonoBehaviour
{
    private Animator _anim;
    private int _onAnimState = Animator.StringToHash("On");
    private int _takeAnimState = Animator.StringToHash("Take");
    private int _offAnimState = Animator.StringToHash("Off");

    void Awake()
    {
        _anim = GetComponent<Animator>();
        if (_anim == null)
        {
            _anim = GetComponentInChildren<Animator>();
            if (_anim == null)
                Debug.LogException(new System.Exception("Can't get Animator component"));
        }
    }

    void Update()
    {
        AnimatorStateInfo stateInfo = _anim.GetCurrentAnimatorStateInfo(0);
        if (stateInfo.shortNameHash == _offAnimState)
            gameObject.SetActive(false);
    }

    public void Reset()
    {
        gameObject.SetActive(true);
        _anim.Play(_onAnimState);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
            _anim.Play(_takeAnimState);
    }
}
