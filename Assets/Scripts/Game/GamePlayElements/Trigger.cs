﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trigger : MonoBehaviour
{
    /// <summary>
    /// Имя анимаций - состояние включено
    /// </summary>
    public string TriggeredAnimName;

    /// <summary>
    /// Список объектов которые должны активироваться
    /// </summary>
    public List<TriggerEnabler> EnableObjects;

    // Аниматор на объекте
    private Animator _animator;

    // Анимация для проигрывания
    private int _animState;

    // Анимация по умолчанию
    private int _defaultAnimState;

    void Start()
    {
        // Если имя анимации не пустое...
        if (!string.IsNullOrEmpty(TriggeredAnimName))
        {
            // ... получить ее хэш
            _animState = Animator.StringToHash(TriggeredAnimName);

            // Попытаться получить аниматор объекта
            _animator = GetComponent<Animator>();
            // Если его нету, попытаться получить аниматор у дочернего объекта
            if (_animator == null)
                _animator = GetComponentInChildren<Animator>();

            // Анимация играемая при старте - будет анимацией по умолчанию
            if (_animator != null)
                _defaultAnimState = _animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // Если мы колидимся с персом
        if (other.tag == "Player")
        {
            // Проиграть анимацию
            if (_animator != null)
                _animator.Play(_animState);

            // Переключить все объекты в списке
            foreach (TriggerEnabler item in EnableObjects)
            {
                item.Enable();
            }
        }
    }

    /// <summary>
    /// Сбросить состояние объекта к изначальному
    /// </summary>
    public void Reset()
    {
        // Проиграть анимацию по умолчанию
        if (_animator != null)
            _animator.Play(_defaultAnimState);
    }
}
