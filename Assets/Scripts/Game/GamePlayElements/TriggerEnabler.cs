﻿using UnityEngine;
using System.Collections;

public class TriggerEnabler : MonoBehaviour
{
    /// <summary>
    /// Флаг: переключен ли объект
    /// </summary>
    public bool IsTriggered { get; set; }

    /// <summary>
    /// Имя анимаций - состояние включено
    /// </summary>
    public string TriggeredAnimName;

    // Аниматор на объекте
    private Animator _animator;

    // Анимация для проигрывания
    private int _animState;

    // Анимация по умолчанию
    private int _defaultAnimState;

    void Start()
    {
        // Если имя анимации не пустое...
        if (!string.IsNullOrEmpty(TriggeredAnimName))
        {
            // ... получить ее хэш
            _animState = Animator.StringToHash(TriggeredAnimName);

            // Попытаться получить аниматор объекта
            _animator = GetComponent<Animator>();
            // Если его нету, попытаться получить аниматор у дочернего объекта
            if (_animator == null)
                _animator = GetComponentInChildren<Animator>();

            // Анимация играемая при старте - будет анимацией по умолчанию
            if (_animator != null)
                _defaultAnimState = _animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
        }

        IsTriggered = false;
    }

    /// <summary>
    /// Переключить объект
    /// </summary>
    public void Enable()
    {
        IsTriggered = true;

        // Проиграть анимацию
        if (_animator != null)
            // Используем не принудительное резкое включение анимации, а включаем переменную в аниматоре
            //_animator.Play(_animState);
            _animator.SetBool("On", true);
    }

    /// <summary>
    /// Сбросить состояние объекта к изначальному
    /// </summary>
    public void Reset()
    {
        IsTriggered = false;
        //Выключаем переменную
        _animator.SetBool("On", false);
        // Проиграть анимацию по умолчанию
        if (_animator != null)
            _animator.Play(_defaultAnimState);
    }
}
