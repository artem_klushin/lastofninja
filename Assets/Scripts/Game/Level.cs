﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour
{
    /// <summary>
    /// Размер камеры для этого уровня
    /// </summary>
    public float CameraSize = 5.0f;

    /// <summary>
    /// Точка куда приближается камера
    /// </summary>
    public Transform CameraPoint;

    // Префаб игрока
    public GameObject PlayerPrefab;

    // текущий инстанс игрока
    private GameObject _currentPlayerIntance;

    // Точка старта на уровне
    private Vector3 _startPoint;

    // Родительский объект для игровых элементов
    private GameObject _gameplayElements;

    // Аниматор рамки
    private Animator _borderAnimator;

    private int _borderSelectAnim = Animator.StringToHash("Select");
    private int _borderSelectCompletedAnim = Animator.StringToHash("SelectComplite");
    private int _borderCompletedAnim = Animator.StringToHash("Complite");

    private SpriteRenderer _levelStatus;

    // Ачивка за все монетки
    private GameObject _coinAchivementIndicator;

    // Ачивка за время
    private GameObject _timeAchivementIndicator;

    // Ачивка за смерть
    private GameObject _deadAchivementIndicator;

    // Текста "Время завершения"
    private TextMesh _compliteTimeText;

    // Информация об уровне
    public LevelInfo Info
    {
        get
        {
            return _info;
        }
        set
        {
            _info = value;
            _coinAchivementIndicator.SetActive(_info.IsCoinsAchivementUnlocked);
            _timeAchivementIndicator.SetActive(_info.IsTimeAchivementUnlocked);
            _deadAchivementIndicator.SetActive(_info.IsDeadAchivementUnlocked);
            if (_info.CompliteTime == 0f)
                _compliteTimeText.text = "";
            else
                _compliteTimeText.text = _info.CompliteTime.ToString();

            if (_info.IsComplite)
                _levelStatus.color = new Color(255f/255f, 216/255f, 0f);
            else
                _levelStatus.color = new Color(58f/255f, 60f/255f, 59f/255f);
        }
    }
    private LevelInfo _info;


    void Start()
    {
        // Изменяем камеру сайз под разрешение
        float width = Screen.width;
        float height = Screen.height;
        float _aspectRatio = width / height;

        CameraSize = (CameraSize * ((1.777f - _aspectRatio)+1.0f));

        Transform tmp;

        CameraPoint = transform.FindChild("CameraPoint");
        if (CameraPoint == null)
            Debug.LogException(new System.Exception("CameraPoint didn't find in " + transform.name));

        if (PlayerPrefab == null)
            Debug.LogException(new System.Exception("Player don't set " + transform.name));

        tmp = transform.FindChild("StartPoint");
        if (tmp == null)
            Debug.LogException(new System.Exception("StartPoint didn't find in " + transform.name));
        else
            _startPoint = tmp.position;

        tmp = transform.FindChild("GamePlayObjects");
        if (tmp == null)
            Debug.LogException(new System.Exception("GamePlayElements didn't find in " + transform.name));
        else
        {
            _gameplayElements = tmp.gameObject;
            _gameplayElements.SetActive(false);
        }

        tmp = transform.FindChild("Border");
        if (tmp != null)
            _borderAnimator = tmp.GetComponent<Animator>();
        else
            Debug.LogException(new System.Exception("Border didn't find in " + transform.name));

        tmp = transform.FindChild("StatusUI/Achive-Coins");
        if (tmp != null)
            _coinAchivementIndicator = tmp.gameObject;
        else
            Debug.LogException(new System.Exception("Achive-Coins didn't find in " + transform.name));

        tmp = transform.FindChild("StatusUI/Achive-Dead");
        if (tmp != null)
            _deadAchivementIndicator = tmp.gameObject;
        else
            Debug.LogException(new System.Exception("Achive-Dead didn't find in " + transform.name));

        tmp = transform.FindChild("StatusUI/Achive-Time");
        if (tmp != null)
            _timeAchivementIndicator = tmp.gameObject;
        else
            Debug.LogException(new System.Exception("Achive-Time didn't find in " + transform.name));

        tmp = transform.FindChild("StatusUI/CompliteTime");
        if (tmp != null)
            _compliteTimeText = tmp.GetComponent<TextMesh>();
        else
            Debug.LogException(new System.Exception("CompliteTime   didn't find in " + transform.name));

        tmp = transform.FindChild("StatusUI/BackGround");
        if (tmp != null)
            _levelStatus = tmp.GetComponent<SpriteRenderer>();
        else
            Debug.LogException(new System.Exception("StatusUI/BackGround didn't find in " + transform.name));
    }

    /// <summary>
    /// Начать уровень
    /// </summary>
    public void Run()
    {
        if (_currentPlayerIntance != null)
        {
            PlayerControl currentPlayer = _currentPlayerIntance.GetComponent<PlayerControl>();
            currentPlayer.Died -= Player_Died;
            currentPlayer.Finish -= Player_Finish;
            DestroyObject(_currentPlayerIntance);
        }

        BroadcastMessage("Reset");

        _currentPlayerIntance = (GameObject)Instantiate(PlayerPrefab, _startPoint, Quaternion.identity);

        PlayerControl Player = _currentPlayerIntance.GetComponent<PlayerControl>();
        Player.Died += Player_Died;
        Player.Finish += Player_Finish;
    }

    /// <summary>
    /// Включить уровень
    /// </summary>
    public void Enable()
    {
        GetComponent<Collider2D>().enabled = false;
        _gameplayElements.SetActive(true);
    }

    /// <summary>
    /// Выключить уровень
    /// </summary>
    public void Disable()
    {
        GetComponent<Collider2D>().enabled = true;
        _gameplayElements.SetActive(false);

        if (_currentPlayerIntance != null)
        {
            PlayerControl currentPlayer = _currentPlayerIntance.GetComponent<PlayerControl>();
            currentPlayer.Died -= Player_Died;
            currentPlayer.Finish -= Player_Finish;
            DestroyObject(_currentPlayerIntance);
        }
    }

    #region Event's handlers

    //------------------------//
    // Игрок завершил уровень //
    //------------------------//
    private void Player_Finish(object sender, System.EventArgs e)
    {
        _info.IsComplite = true;
        OnFinish();
    }

    //------------//
    // Игрок умер //
    //------------//
    private void Player_Died(object sender, System.EventArgs e)
    {
        OnDied();
    }

    #endregion

    #region Event's

    /// <summary>
    /// Смерть
    /// </summary>
    public event System.EventHandler<System.EventArgs> Died;
    private void OnDied()
    {
        System.EventHandler<System.EventArgs> handler = Died;
        if (handler != null)
            handler(this, new System.EventArgs());
    }

    /// <summary>
    /// Финиш
    /// </summary>
    public event System.EventHandler<System.EventArgs> Finish;
    private void OnFinish()
    {
        System.EventHandler<System.EventArgs> handler = Finish;
        if (handler != null)
            handler(this, new System.EventArgs());
    }

    #endregion
}
