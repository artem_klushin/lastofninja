﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{

    public static PlayerControl Instance;

    /// <summary>
    /// Скорость передвижения
    /// </summary>
    public float MoveSpeed = 20f;

    /// <summary>
    /// Макс. скорость передвижения
    /// </summary>
    public float MaxMoveSpeed = 30f;

    /// <summary>
    /// Скорость передвижения в полете
    /// </summary>
    public float FlySpeed = 10f;

    /// <summary>
    /// Макс. высота подъема в градусах
    /// по которому можно бежать
    /// </summary>
    public float MaxClimbAngle = 60f;

    /// <summary>
    /// Сила прыжка
    /// </summary>
    public float JumpForce = 50f;

    /// <summary>
    /// Макс. сила прыжка
    /// </summary>
    public float MaxJumpForce = 5f;

    /// <summary>
    /// Сила прыжка от стены
    /// </summary>
    public float JumpFromWallOffset = 100f;

    /// <summary>
    /// Время между тапами для прыжка
    /// </summary>
    public double DoubleTapMiliseconds = 300;

    /// <summary>
    /// Может ли пользователь управлять персонажем
    /// </summary>
    public bool CanInput { get; set; }

    // Объект для проверки косания земли
    private Transform _groundCheckerTransform;

    // Объект для проверки косания стены справа
    private Transform _rightCheckerTransform;

    // Наш rigidbody
    private Rigidbody2D _rigidbody;

    // Части ragdoll
    private GameObject _ragdoll;

    private Animator _animator;

    // Переменная для камеры интерфейса
    public GameObject _interfaceCamera;

    //Переменная для назначения системы частиц. "Появление"
    public Transform fxAppear;

    //Переменная переключатель для системы частиц "Появление"
    private ParticleSystem fxAppearSwich;

    //Переменная для назначения системы частиц. "Финиш"
    public Transform fxDisappear;

    //Переменная переключатель для системы частиц "Финиш"
    private ParticleSystem fxDisappearSwich;

    //Тип управления
    //0-Windows
    //1-AndroidIos
    private int ControlType = 0;

    private bool moveDirectionLeft;


    private int _animIdleState = Animator.StringToHash("Idle");
    private int _animRunState = Animator.StringToHash("Run");
    private int _animJumpState = Animator.StringToHash("Jump");
    private int _animFallState = Animator.StringToHash("Fall");
    private int _animSlideState = Animator.StringToHash("Slide");
    private int _animFinish = Animator.StringToHash("Ch-Ok");

    // Флаг: Стоим ли мы на земле?
    private bool _isGrounded = false;

    // Флаг: можно ли прыгнуть?
    private bool _canJump = false;

    // Флаг: скользим ли мы по стене?
    private bool _isSliding = false;

    // Флаг: завершен ли уровень?
    private bool _isFinished = false;

    // Временная метка для определения двойного тапа
    //private System.DateTime _lastTouchTime = System.DateTime.Now; НЕ ИСПОЛЬЗУЕТСЯ ДВОЙНОЙ ТАП

    //Переменная для того чтобы снимать ограничение мощности прыжка, когда работает пружина
    private bool springAssumption;

    //Определяем направление прыжка
    private float jumpCorrect;
    private float jumpTimer=30;

    //Таймер для прыжка от стены
    private float _wallJumpTimer = 10;

    //
    private bool _groundCollide = true;



    void Awake()
    {
       
        //находим интерфейс камеру, чтобы определять нажатия на кнопки.
        _interfaceCamera = GameObject.Find("InterfaceCamera");

        //Назначаем систему частиц на появление
        fxAppearSwich = fxAppear.GetComponent<ParticleSystem>();

        //Назначаем систему частиц на финиш
        fxDisappearSwich = fxDisappear.GetComponent<ParticleSystem>();

        Instance = this;
        _groundCheckerTransform = transform.Find("GroundChecker").transform;
        _rightCheckerTransform = transform.Find("RightChecker").transform;
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponentInChildren<Animator>();

        // Поиск коллайдеров ragdoll
        _ragdoll = transform.Find("RagDoll").gameObject;

        CanInput = true;
        // Включаем эффект при появлении перса.
        fxAppearSwich.Play();
    }

    //Снятие ограничения на мощность прыжка у объекта спринг



    void Update()
    {
        //Когда пацан летит вниз выключаю переменную, которая выключает ограничение в высоте прыжка
        if (_rigidbody.velocity.y < 0.1f)
        {
            springAssumption = false;
        }

        //Двигаю граунд чекер
        if (transform.rotation.eulerAngles.z > 1 || transform.rotation.eulerAngles.z < 1 && _isSliding)
        {
           _groundCheckerTransform.transform.localPosition = new Vector3(0f, -0.2f, 0);
        }

        if (!_isSliding && transform.rotation.eulerAngles.z == 0 || _rigidbody.velocity.y<-0.0f )
        {
            _groundCheckerTransform.transform.localPosition = new Vector3(0.09f, -0.2f, 0);
        }


        // Если не скользим по стене, то разворачиваемся
        if (!_isSliding)
        {
            // Разворачиваемся в сторону направления движения
            if (_rigidbody.velocity.x < -0.1f)
                transform.localScale = new Vector3(-1.5f, 1.5f, 1.5f);
            else if (_rigidbody.velocity.x > 0.1f)
                transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }

        // Проверка земли
        _isGrounded = Physics2D.Linecast(transform.position, _groundCheckerTransform.position, 1 << LayerMask.NameToLayer("Ground"));

        // Если мы в воздухе, то вернуться в вертикальное положение
        if (!_isGrounded || _isSliding)
        {
            transform.eulerAngles = Vector3.zero;
        }

        else
        // Повернуться перепендикулярно наклонной поверхности
        {
            
                RaycastHit2D hit = Physics2D.Linecast(transform.position, _groundCheckerTransform.position, 1 << LayerMask.NameToLayer("Ground"));

                float rotAngle = Vector2.Angle(Vector2.up, hit.normal);
                if (MaxClimbAngle <= rotAngle)
                    rotAngle = 0;

                if (hit.normal.x < 0)
                    transform.eulerAngles = new Vector3(0, 0, rotAngle);
                else
                    transform.eulerAngles = new Vector3(0, 0, -rotAngle);
            
        }

        if (CanInput)
        {
            /*if (Input.GetKeyDown(KeyCode.Space) && (_isGrounded || _isSliding))
                _canJump = true;

            if (Input.GetKeyUp(KeyCode.Space))
                _canJump = false;*/

            /*
            #if UNITY_STANDALONE_WIN || UNITY_EDITOR
                        if (Input.GetMouseButtonDown(0) && (_isGrounded || _isSliding))
                        {
                            System.TimeSpan deltaTimeTouch = System.DateTime.Now - _lastTouchTime;

                            if (deltaTimeTouch.TotalMilliseconds < DoubleTapMiliseconds)
                                _canJump = true;

                            _lastTouchTime = System.DateTime.Now;
                        }
            #endif
            

            /* Первый тип управления
        #if UNITY_ANDROID
                    if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && (_isGrounded || _isSliding))
                {
                    System.TimeSpan deltaTimeTouch = System.DateTime.Now - _lastTouchTime;

                    if (deltaTimeTouch.TotalMilliseconds < DoubleTapMiliseconds)
                        _canJump = true;

                    _lastTouchTime = System.DateTime.Now;
                }
        #endif
            */
        }
    // Функция анимаций на пацане
    ManageAnimation();
    }

    void FixedUpdate()
    {
        //Debug.Log(_wallJumpTimer);

        if((_isSliding || _isGrounded) && _wallJumpTimer < 10)
        {
            //Обнуляем таймер прыжка от стены
            _wallJumpTimer = 10;
        }
        if (CanInput)
        {

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
            // ускорение влево
            if (Input.GetKey(KeyCode.LeftArrow))
                MoveLeft();

            // ускорение вправо
            if (Input.GetKey(KeyCode.RightArrow))
                MoveRight();
            if (Input.GetKey(KeyCode.Space) && _wallJumpTimer > 0 )
                _canJump = true;
            if (Input.GetKeyUp(KeyCode.Space))
                _canJump = false;
#endif

            /*
            #if UNITY_STANDALONE_WIN || UNITY_EDITOR
                        if (Input.GetMouseButton(0))
                        {
                            Vector3 clickPos = Input.mousePosition;
                            if (clickPos.y < Screen.height / 2)
                            {
                                if (clickPos.x < Screen.width / 2)
                                    MoveLeft();
                                else
                                    MoveRight();
                            }
                            else if (_isGrounded || _isSliding)
                            {
                                _canJump = true;
                            }
                        }
            #endif
            
            */

            /* Первый тип управления
            #if UNITY_ANDROID
                    if (Input.touchCount > 0)
                    {
                        for (int i = 0; i < Input.touchCount; ++i)
                        { 
                        Vector2 touchPos = Input.GetTouch(i).position;

                        if (touchPos.y < Screen.height / 2)
                        {
                            if (touchPos.x < Screen.width / 2)
                                MoveLeft();
                            else
                                MoveRight();
                        }
                        else if (_isGrounded || _isSliding && Input.touchCount > 1)
                        {
                            _canJump = true;
                        }

                        }
                    }
                    if (Input.touchCount < 1)
                    { _canJump = false; }
            #endif
            */

            /////////////////////////
            //Второй тип управления//
            /////////////////////////
#if UNITY_ANDROID
            if (Input.touchCount>0)
            {
                if (_interfaceCamera.GetComponent <ControlType>().LeftMoveButton)
                {
                    MoveLeft();
                   
                }

                if (_interfaceCamera.GetComponent<ControlType>().RightMoveButton)
                {
                    MoveRight();
                }

                if (_interfaceCamera.GetComponent<ControlType>().JumpButton && _wallJumpTimer > 0)
                {
                    _canJump = true;
                }

            }

            if (!_interfaceCamera.GetComponent<ControlType>().JumpButton && _canJump && ControlType == 1)
            {
                _canJump = false;
            }


        }
        // Тормозим пацана на земле, если влево и вправо не нажаты
        if (!_isSliding 
            && _isGrounded && Mathf.Abs(_rigidbody.velocity.x) > 0.1f 
            && (!_interfaceCamera.GetComponent<ControlType>().LeftMoveButton && !_interfaceCamera.GetComponent<ControlType>().RightMoveButton) 
            && ControlType == 1)
        {
            _rigidbody.AddForce(new Vector2(_rigidbody.velocity.x * -3.0f, 0.0f), ForceMode2D.Force);
        }
#endif

#if UNITY_STANDALONE_WIN || UNITY_EDITOR

        if (!_isSliding 
            && _isGrounded 
            && Mathf.Abs(_rigidbody.velocity.x) > 0.1f 
            && (!Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow))
            && ControlType == 0)
        {
            _rigidbody.AddForce(new Vector2(_rigidbody.velocity.x * -3.0f, 0.0f ), ForceMode2D.Force);
            
        }
#endif
        if (!_isSliding && !_isGrounded && _wallJumpTimer>0.0f)
        {
            wallJumpTimer();
        }

        // если нужно прыгнуть
        if (_canJump)
    {
            jumpTimer = jumpTimer - 1.0f;

        if (_isSliding)
        {
            float direction = 0f;
            if (transform.localScale.x < 0)
                direction += JumpFromWallOffset;
            else
                direction -= JumpFromWallOffset;

            _rigidbody.gravityScale = 1.5f;

            // вертикальое ускорение
            if (_rigidbody.velocity.y < MaxJumpForce)

                _rigidbody.AddForce(new Vector2(direction, JumpForce * 5), ForceMode2D.Force);
        }

            if (!_isSliding)
            {
                // вертикальое ускорение
                if (Mathf.Abs(_rigidbody.velocity.y) < MaxJumpForce)
                {

                    if (transform.eulerAngles.z < 180)
                    { jumpCorrect = transform.eulerAngles.z * -1; }
                    else
                        jumpCorrect = 360 - transform.eulerAngles.z;

                    _rigidbody.AddForce(new Vector2(jumpCorrect, JumpForce * 2));
                }
            }
            
    } // End CanJump

    else {
    jumpTimer = 30;
    }

        // Ограничение скорости до максимальной
        if (Mathf.Abs(_rigidbody.velocity.x) > MaxMoveSpeed)
        {
            _rigidbody.velocity = new Vector2(Mathf.Sign(_rigidbody.velocity.x) * MaxMoveSpeed, _rigidbody.velocity.y);
        }


        if ((Mathf.Abs(_rigidbody.velocity.y) > MaxJumpForce || jumpTimer == 0) && !springAssumption || _groundCollide)
        {
            _canJump = false;
            //Debug.Log("Blocked Velocity");
             //_rigidbody.velocity = new Vector2(_rigidbody.velocity.x, Mathf.Sign(_rigidbody.velocity.y) * MaxJumpForce);
        }

    }



//-----------------------//
// Управление анимациями //
//-----------------------//
void ManageAnimation()
{
    if (_isFinished)
    {
        _animator.Play(_animFinish);
        return;
    }

    if (_isGrounded)
    {
        bool isInpuActive = false;

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        if (Input.GetMouseButton(0) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
            isInpuActive = true;

#endif

#if UNITY_ANDROID
        if (Input.touchCount > 0)
            isInpuActive = true;
#endif

        if (isInpuActive && Mathf.Abs(_rigidbody.velocity.x) > 0.01) //Пашок добавил, а тут, чтобы когда он втыкается в стену при нажатой кнопки не застывал в позе бега
        {
            _animator.SetBool(_animRunState, true);
            _animator.speed = Mathf.Abs(_rigidbody.velocity.x * 0.3f); // Пашок добавил, чтобы скорост ьвоспроизведения анимации зависила от скорости передвижения перса

            _animator.SetBool(_animIdleState, false);
            _animator.SetBool(_animFallState, false);
            _animator.SetBool(_animJumpState, false);
            _animator.SetBool(_animSlideState, false);
        }
        else
        {
            _animator.speed = 1.0f; // Пашок добавил, возвращаем скорость воспроизведения до нормы

            // Если стоим - то включить анимацию ожидания
            if (Mathf.Abs(_rigidbody.velocity.x) < 0.1f && _isGrounded)
                _animator.SetBool(_animIdleState, true);
            // иначе если тормозим или скользим по склону, торомозить
            else if (Mathf.Abs(_rigidbody.velocity.x) > 2.0f && Mathf.FloorToInt(transform.eulerAngles.z) != 0)
                _animator.SetBool(_animIdleState, false);
            _animator.SetBool(_animRunState, false);
            _animator.SetBool(_animFallState, false);
            _animator.SetBool(_animJumpState, false);
            _animator.SetBool(_animSlideState, false);
        }
    }
    else
    {
        _animator.speed = 1.0f; // Пашок добавил, возвращаем скорость воспроизведения до нормы
        if (_rigidbody.velocity.y > 0 && !_isGrounded)
        {
            _animator.SetBool(_animJumpState, true);

            _animator.SetBool(_animIdleState, false);
            _animator.SetBool(_animRunState, false);
            _animator.SetBool(_animFallState, false);
            _animator.SetBool(_animSlideState, false);
        }
        else if (!_isSliding && _rigidbody.velocity.y < 0)
        {
            _animator.SetBool(_animFallState, true);

            _animator.SetBool(_animIdleState, false);
            _animator.SetBool(_animRunState, false);
            _animator.SetBool(_animJumpState, false);
            _animator.SetBool(_animSlideState, false);
        }
        else if (_isSliding)
        {
            _animator.SetBool(_animSlideState, true);

            _animator.SetBool(_animIdleState, false);
            _animator.SetBool(_animRunState, false);
            _animator.SetBool(_animFallState, false);
            _animator.SetBool(_animJumpState, false);
        }
    }
}

void OnTriggerEnter2D(Collider2D other)
{
    if (other.tag == "TimeCoin")
    {
        TimeLine.Instance.IncreaseTime();
    }
    else if (other.tag == "FinishDoor")
    {
        CanInput = false;
        _isFinished = true;
        OnFinish();
    }
    if (other.tag == "Spring")
    {
        springAssumption = true;
        //Debug.Log(springAssumption);
    }

}

void OnTriggerStay2D(Collider2D other)
{   
    //Debug.Log(Mathf.Abs(_rigidbody.velocity.y));
    if (other.gameObject.layer != LayerMask.NameToLayer("Ground"))
        return;
        
        //Спускается со стены
        _isSliding = true;


    _rigidbody.gravityScale = 1.5f;

    if (_rigidbody.velocity.y >= 0.0f) //Пашок говна добавил Чтобы сучка останавливать, когда он на стену прилетает
    {   
        _rigidbody.velocity = new Vector2(_rigidbody.velocity.x * 0.0f, _rigidbody.velocity.y * 0.99f);
    }

    if (_rigidbody.velocity.y <= 0.0f) //Пашок говна добавил Чтобы сучка останавливать, когда он на стену прилетает
    {
        _rigidbody.velocity = new Vector2(_rigidbody.velocity.x * 0.0f, _rigidbody.velocity.y * 0.65f); //Пашок говна добавил 

    }
    //Debug.Log(_rigidbody.velocity.y);
}

void OnTriggerExit2D(Collider2D other)
{
    if (other.gameObject.layer != LayerMask.NameToLayer("Ground"))
        return;

    _isSliding = false;
    _rigidbody.gravityScale = 1.5f;
}

void OnCollisionEnter2D(Collision2D coll)
{
        if (coll.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            _canJump = false;
            _groundCollide = true;

        }
            if (coll.gameObject.tag == "Mine")
    {
        CanInput = false;
        EnableRagdoll();
        OnDied();
    }
}
    void OnCillisionExit()
    {
        _groundCollide = false;
    }

void OnCollisionStay(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            _canJump = false;

        }
    }

    //Запускаем таймер на прыжок от стены
void wallJumpTimer()
    {
            _wallJumpTimer = _wallJumpTimer - 1;
    }

//----------------//
// Движение влево //
//----------------//
void MoveLeft()
{
        
        transform.localScale = new Vector3(-1.5f, 1.5f, 1.5f);
    if (_rigidbody.velocity.x < MaxMoveSpeed)
    {
        
        Vector2 heading = _rightCheckerTransform.position - transform.position;
        float distance = heading.magnitude;
        Vector2 direction = heading / distance; // This is now the normalized direction.
            if (!moveDirectionLeft && _isGrounded) // Тушим энерцию
            {
                _rigidbody.AddForce(-_rigidbody.velocity/5, ForceMode2D.Impulse);
            }
        if (_isGrounded)
            _rigidbody.AddForce(direction * MoveSpeed, ForceMode2D.Force);
        else
            _rigidbody.AddForce(direction * FlySpeed, ForceMode2D.Force);
        }
        moveDirectionLeft = true;
    }

//-----------------//
// Движение вправо //
//-----------------//
void MoveRight()
{
    
        transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
    if (_rigidbody.velocity.x < MaxMoveSpeed)
    {
        
        Vector2 heading = _rightCheckerTransform.position - transform.position;
        float distance = heading.magnitude;
        Vector2 direction = heading / distance; // This is now the normalized direction.
            if (moveDirectionLeft && _isGrounded)// Тушим энерцию
            {
                _rigidbody.AddForce(-_rigidbody.velocity/5, ForceMode2D.Impulse);
            }
        if (_isGrounded)
            _rigidbody.AddForce(direction * MoveSpeed, ForceMode2D.Force);
        else
            _rigidbody.AddForce(direction * FlySpeed, ForceMode2D.Force);
        
    }
        
        moveDirectionLeft = false;
}

void EnableRagdoll()
{
    _ragdoll.SetActive(true);

    // Отключение основных коллайдеров
    Collider2D[] mainColliders = GetComponents<Collider2D>();
    foreach (Collider2D item in mainColliders)
    {
        item.enabled = false;
    }

    // Отключение аниматора и удаление rigidbody
    _animator.gameObject.SetActive(false);
    this.enabled = false;
    Destroy(_rigidbody);
}

#region Event's

/// <summary>
/// Смерть
/// </summary>
public event System.EventHandler<System.EventArgs> Died;
private void OnDied()
{
    System.EventHandler<System.EventArgs> handler = Died;
    if (handler != null)
        handler(this, new System.EventArgs());
}

/// <summary>
/// Финиш
/// </summary>
public event System.EventHandler<System.EventArgs> Finish;
private void OnFinish()
{
    //Включаем эффект исчезновения
    fxDisappearSwich.Play();
    // Отключение основных коллайдеров
    Collider2D[] mainColliders = GetComponents<Collider2D>();
    foreach (Collider2D item in mainColliders)
    {
        item.enabled = false;
    }

    // Отключение аниматора и удаление rigidbody
    _animator.gameObject.SetActive(false);
    this.enabled = false;
    Destroy(_rigidbody);

    System.EventHandler<System.EventArgs> handler = Finish;
    if (handler != null)
        handler(this, new System.EventArgs());
}

#endregion
}
