﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerRagDoll : MonoBehaviour
{
    public Transform[] BodyParts;
    public Transform[] BodyPartsLinks;
    //public Collider2D[] BodyColliders;
    //public List<Collider2D> RagDollColliders=BodyParts;

    void Awake()
    {
        if (BodyParts.Length != BodyPartsLinks.Length)
            Debug.LogException(new System.Exception("BodyParts not set"));
    }

    void OnEnable()
    {
        for (int i = 0; i < BodyParts.Length; i++)
        {
            if (i == BodyParts.Length)
            {
                break;
            }
            BodyParts[i].position = BodyPartsLinks[i].position;
            BodyParts[i].position = BodyPartsLinks[i].position;
        }
    }
}
