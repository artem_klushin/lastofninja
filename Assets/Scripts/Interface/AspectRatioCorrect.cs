﻿using UnityEngine;
using System.Collections;

public class AspectRatioCorrect : MonoBehaviour {

    private float _aspectRatio=0.0f;
    public GameObject GameCamera;
    public GameObject TimeLineScalePivot;
    public GameObject UICustomPos;
    public GameObject UIMenuPos;
    public GameObject Info;
    public GameObject Sectors;
    public GameObject UIButtonJump;
    public GameObject UIMove;


    //5:4 = 1.25
    //4:3 = 1.333
    //3:2 = 1.5
    //16:10 = 1.6
    //16:9 = 1.777


    void Start () {

        //Измеряем аспект ратио
        float width = Screen.width;
        float height = Screen.height;
        _aspectRatio = width / height;
        //Debug.Log(_aspectRatio);

        // 16:9 OK
        if (_aspectRatio>1.65 && _aspectRatio < 1.8)
        {
            GameCamera.transform.localPosition = new Vector3(0, -12.5f, -10);
            GameCamera.GetComponent<Camera>().orthographicSize = 45;
            TimeLineScalePivot.transform.localScale = new Vector3(1.08f, 1.08f, 1.08f);
            UICustomPos.transform.localPosition = new Vector3(-8.91f, 5.02f,0); 
            UIMenuPos.transform.localPosition = new Vector3(8.91f, 5.02f, 0); 
            Info.transform.localPosition = new Vector3(0, 20.7f, 0); 
            Sectors.transform.localPosition = new Vector3(0, -36.0f, 0);
            UIMove.transform.localPosition = new Vector3(-6, 0, 0);
            UIButtonJump.transform.localPosition = new Vector3(7.5f, 0, 0);
        }
        // 16:10
        if (_aspectRatio > 1.55 && _aspectRatio < 1.65)
        {
            GameCamera.transform.localPosition = new Vector3(0, 34.0f, -10);//
            GameCamera.GetComponent<Camera>().orthographicSize = 58;//
            TimeLineScalePivot.transform.localScale = new Vector3(0.96f, 0.96f, 0.96f);//
            UICustomPos.transform.localPosition = new Vector3(-8.0f, 5.02f, 0);//
            UIMenuPos.transform.localPosition = new Vector3(8.01f, 5.02f, 0);//
            Info.transform.localPosition = new Vector3(0, 74.5f, 0);//
            Sectors.transform.localPosition = new Vector3(0, 41.3f, 0);//
            UIMove.transform.localPosition = new Vector3(-5.45f, 0, 0);//
            UIButtonJump.transform.localPosition = new Vector3(6.8f, 0, 0);//
        }
        // 3:2
        if (_aspectRatio > 1.45 && _aspectRatio < 1.55)
        {
            GameCamera.transform.localPosition = new Vector3(0, 37.2f, -10);//
            GameCamera.GetComponent<Camera>().orthographicSize = 62;//
            TimeLineScalePivot.transform.localScale = new Vector3(0.89f, 0.89f, 0.89f);//
            UICustomPos.transform.localPosition = new Vector3(-7.5f, 5.02f, 0);//
            UIMenuPos.transform.localPosition = new Vector3(7.5f, 5.02f, 0);//
            Info.transform.localPosition = new Vector3(0, 78.8f, 0);//
            Sectors.transform.localPosition = new Vector3(0, 41.6f, 0);//
            UIMove.transform.localPosition = new Vector3(-4.96f, 0, 0);//
            UIButtonJump.transform.localPosition = new Vector3(6.38f, 0, 0);//
        }
        // 4:3
        if (_aspectRatio > 1.3 && _aspectRatio < 1.45)
        {
            GameCamera.transform.localPosition = new Vector3(0, 37.2f, -10);//
            GameCamera.GetComponent<Camera>().orthographicSize = 68;//
            TimeLineScalePivot.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);//
            UICustomPos.transform.localPosition = new Vector3(-6.66f, 5.02f, 0);//
            UIMenuPos.transform.localPosition = new Vector3(6.66f, 5.02f, 0);//
            Info.transform.localPosition = new Vector3(0, 84.5f, 0);//
            Sectors.transform.localPosition = new Vector3(0, 47.5f, 0);//
            UIMove.transform.localPosition = new Vector3(-4.67f, 0, 0);//
            UIMove.transform.localScale = new Vector3(0.73f, 0.73f, 0.73f);//
            UIButtonJump.transform.localPosition = new Vector3(5.752f, 0, 0);//
            UIButtonJump.transform.localScale = new Vector3(0.36f, 0.36f, 0.36f);
        }
        // 5:4
        if (_aspectRatio > 1.2 && _aspectRatio < 1.3)
        {
            GameCamera.transform.localPosition = new Vector3(0, 41.0f, -10);//
            GameCamera.GetComponent<Camera>().orthographicSize = 72;//
            TimeLineScalePivot.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);//
            UICustomPos.transform.localPosition = new Vector3(-6.25f, 5.02f, 0);//
            UIMenuPos.transform.localPosition = new Vector3(6.25f, 5.02f, 0);//
            Info.transform.localPosition = new Vector3(0, 88.8f, 0);//
            Sectors.transform.localPosition = new Vector3(0, 47.8f, 0);//
            UIMove.transform.localPosition = new Vector3(-4.23f, 0, 0);//
            UIMove.transform.localScale = new Vector3(0.73f, 0.73f, 0.73f);//
            UIButtonJump.transform.localPosition = new Vector3(5.39f, 0, 0);//
            UIButtonJump.transform.localScale = new Vector3(0.36f, 0.36f, 0.36f);//
        }



    }
	
}
