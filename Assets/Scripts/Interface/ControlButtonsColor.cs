﻿using UnityEngine;
using System.Collections;

public class ControlButtonsColor : MonoBehaviour {
    public GameObject left;
    public GameObject Right;
    public GameObject Jump;
    public Color Enable;
    public Color Disable;

    private GameObject _interfaceCamera;


    // Use this for initialization
    void Start () {
     _interfaceCamera = GameObject.Find("InterfaceCamera");

    }
	
	// Update is called once per frame
	void Update () {

        if (_interfaceCamera.GetComponent<ControlType>().LeftMoveButton)
            left.GetComponent<SpriteRenderer>().color = Enable;
        else
            left.GetComponent<SpriteRenderer>().color = Disable;


        if (_interfaceCamera.GetComponent<ControlType>().RightMoveButton)
            Right.GetComponent<SpriteRenderer>().color = Enable;
        else
            Right.GetComponent<SpriteRenderer>().color = Disable;


        if (_interfaceCamera.GetComponent<ControlType>().JumpButton)
            Jump.GetComponent<SpriteRenderer>().color = Enable;
        else
            Jump.GetComponent<SpriteRenderer>().color = Disable;


    }
       
    }

