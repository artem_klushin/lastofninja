﻿using UnityEngine;
using System.Collections;

public class Interface : MonoBehaviour
{
    public enum STATE
    {
        NONE,
        SELECT_LEVEL,
        TAP_TO_START_GAME,
        TAP_TO_RESTART,
        TAP_TO_NEXT_LEVEL
    }

    private GameObject _selectLevel;
    private GameObject _startGame;
    private GameObject _restartGame;
    private GameObject _nextLevel;

    private STATE _currentState;

    private Camera _camera;

    public static Interface Instance;

    void Awake()
    {
        Instance = this;

        Transform tmp;

        tmp = transform.Find("InterfaceCamera");
        if (tmp != null)
            _camera = tmp.GetComponent<Camera>();
        else
            Debug.LogException(new System.Exception("Can't find InterfaceCamera"));

        tmp = transform.Find("GamePlay/SelectLevel");
        if (tmp != null)
        {
            _selectLevel = tmp.gameObject;
            _selectLevel.SetActive(false);
        }
        else
            Debug.LogException(new System.Exception("Can't find SelectLevel"));

        tmp = transform.Find("GamePlay/StartGame");
        if (tmp != null)
        {
            _startGame = tmp.gameObject;
            _startGame.SetActive(false);
        }
        else
            Debug.LogException(new System.Exception("Can't find StartGame"));

        tmp = transform.Find("GamePlay/RestartGame");
        if (tmp != null)
        {
            _restartGame = tmp.gameObject;
            _restartGame.SetActive(false);
        }
        else
            Debug.LogException(new System.Exception("Can't find RestartGame"));

        tmp = transform.Find("GamePlay/NextLevel");
        if (tmp != null)
        {
            _nextLevel = tmp.gameObject;
            _nextLevel.SetActive(false);
        }
        else
            Debug.LogException(new System.Exception("Can't find NextLevel"));
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
            Click();
    }

    public void SetState(STATE state)
    {
        // выключение текущего состояния
        switch(_currentState)
        {
            case STATE.SELECT_LEVEL:
                _selectLevel.SetActive(false);
                break;
            case STATE.TAP_TO_RESTART:
                _restartGame.SetActive(false);
                break;
            case STATE.TAP_TO_START_GAME:
                _startGame.SetActive(false);
                break;
            case STATE.TAP_TO_NEXT_LEVEL:
                _nextLevel.SetActive(false);
                break;
        }

        // включение запрашиваемого состояния
        switch (state)
        {
            case STATE.SELECT_LEVEL:
                _selectLevel.SetActive(true);
                break;
            case STATE.TAP_TO_RESTART:
                _restartGame.SetActive(true);
                break;
            case STATE.TAP_TO_START_GAME:
                _startGame.SetActive(true);
                break;
            case STATE.TAP_TO_NEXT_LEVEL:
                _nextLevel.SetActive(true);
                break;
        }

        _currentState = state;
    }

    private void Click()
    {
        RaycastHit2D hit = Physics2D.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), Vector2.down);
        if (hit)
        {
            if (hit.transform.name == "StartGame")
            {
                SetState(STATE.NONE);
                OnStartGame();
            }
            else if (hit.transform.name == "RestartGame")
            {
                SetState(STATE.NONE);
                OnRestartGame();
            }
            else if (hit.transform.name == "NextLevel")
            {
                SetState(STATE.NONE);
                OnNextLevel();
            }
        }
    }

    #region Events

    /// <summary>
    /// Событие нажатия рестарта
    /// </summary>
    public event System.EventHandler<System.EventArgs> RestartGame;
    private void OnRestartGame()
    {
        System.EventHandler<System.EventArgs> handler = RestartGame;
        if (handler != null)
            handler(this, new System.EventArgs());
    }

    /// <summary>
    /// Событие начала игры
    /// </summary>
    public event System.EventHandler<System.EventArgs> StartGame;
    private void OnStartGame()
    {
        System.EventHandler<System.EventArgs> handler = StartGame;
        if (handler != null)
            handler(this, new System.EventArgs());
    }

    /// <summary>
    /// Событие следующий уровень
    /// </summary>
    public event System.EventHandler<System.EventArgs> NextLevel;
    private void OnNextLevel()
    {
        System.EventHandler<System.EventArgs> handler = NextLevel;
        if (handler != null)
            handler(this, new System.EventArgs());
    }

    #endregion
}
