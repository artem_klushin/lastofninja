﻿using UnityEngine;
using System.Collections;

public class PackageButton : MonoBehaviour
{
    public PackageInfo Info
    {
        get
        {
            return _info;
        }
        set
        {
            _info = value;
            _numberTextMesh.text = _info.Number.ToString();
            SwitchPackageState(_info.State);
            foreach (LevelInfo item in _info.Levels)
            {
                SpriteRenderer itemRenderer = null;

                if (item.Number == 1)
                {
                    itemRenderer = _firstLevelStatus;
                }
                else if (item.Number == 2)
                    itemRenderer = _secondLevelStatus;
                else if (item.Number == 3)
                    itemRenderer = _thirdLevelStatus;
                else if (item.Number == 4)
                    itemRenderer = _fourthLevelStatus;

                if (item.IsComplite)
                    itemRenderer.color = AvaibleColor;
                else
                    itemRenderer.color = UnavaibleColor;
            }
        }
    }
    private PackageInfo _info;

    public bool Selected
    {
        set
        {
            if (value)
                _selectAnimator.Play(_selectAnim);
            else
                _selectAnimator.Play(_unselectAnim);
        }
    }

    private Animator _selectAnimator;

    private int _selectAnim = Animator.StringToHash("Appear");
    private int _unselectAnim = Animator.StringToHash("Disappear");
    private int _selectOffAnim = Animator.StringToHash("Off");

    private Animator _buttonStatusAnimator;

    private int _avaibleAnim = Animator.StringToHash("Avilible");
    private int _compliteAnim = Animator.StringToHash("Complite");
    private int _lockedAnim = Animator.StringToHash("Blocked");
    private int _unavaibleAnim = Animator.StringToHash("Locked");

    public Color AvaibleColor = new Color(255, 216, 0);
    public Color UnavaibleColor = new Color(104, 118, 116);

    private SpriteRenderer _firstLevelStatus;
    private SpriteRenderer _secondLevelStatus;
    private SpriteRenderer _thirdLevelStatus;
    private SpriteRenderer _fourthLevelStatus;

    private TextMesh _numberTextMesh;

    void Awake()
    {
        Transform tmp;

        tmp = transform.FindChild("Selected");
        if (tmp != null)
            _selectAnimator = tmp.GetComponent<Animator>();
        else
            Debug.LogException(new System.Exception("Can't find object Selected"));

        tmp = transform.FindChild("LevelButton-Anim");
        if (tmp != null)
            _buttonStatusAnimator = tmp.GetComponent<Animator>();
        else
            Debug.LogException(new System.Exception("Can't find object LevelButton-Anim"));

        tmp = transform.FindChild("LevelStatus/LevelStatus-01");
        if (tmp != null)
            _firstLevelStatus = tmp.GetComponent<SpriteRenderer>();
        else
            Debug.LogException(new System.Exception("Can't find object LevelStatus/LevelStatus-01"));

        tmp = transform.FindChild("LevelStatus/LevelStatus-02");
        if (tmp != null)
            _secondLevelStatus = tmp.GetComponent<SpriteRenderer>();
        else
            Debug.LogException(new System.Exception("Can't find object LevelStatus/LevelStatus-02"));

        tmp = transform.FindChild("LevelStatus/LevelStatus-03");
        if (tmp != null)
            _thirdLevelStatus = tmp.GetComponent<SpriteRenderer>();
        else
            Debug.LogException(new System.Exception("Can't find object LevelStatus/LevelStatus-03"));

        tmp = transform.FindChild("LevelStatus/LevelStatus-04");
        if (tmp != null)
            _fourthLevelStatus = tmp.GetComponent<SpriteRenderer>();
        else
            Debug.LogException(new System.Exception("Can't find object LevelStatus/LevelStatus-04"));

        tmp = transform.FindChild("LevelButton-Anim/NumberPivot/Number");
        if (tmp != null)
            _numberTextMesh = tmp.GetComponent<TextMesh>();
        else
            Debug.LogException(new System.Exception("Can't find object LevelButton-Anim/NumberPivot/Number"));
    }

    private void SwitchPackageState(PACKAGE_STATE state)
    {
        switch (state)
        {
            case PACKAGE_STATE.AVAIBLE:
                _buttonStatusAnimator.Play(_avaibleAnim);
                break;
            case PACKAGE_STATE.COMPLITE:
                _buttonStatusAnimator.Play(_compliteAnim);
                break;
            case PACKAGE_STATE.LOCKED:
                _buttonStatusAnimator.Play(_compliteAnim);
                break;
            case PACKAGE_STATE.UNAVAIBLE:
                _buttonStatusAnimator.Play(_unavaibleAnim);
                break;
        }
    }
}
