﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;

public class PackageManager : MonoBehaviour
{

    /// <summary>
    /// Доступ к менеджеру
    /// </summary>
    public static PackageManager Instance;

    /// <summary>
    /// Информация для текущего загруженного пакеджа
    /// </summary>
    public PackageInfo CurrentPackageInfo
    {
        get
        {
            return _structuresList[0].Packages[_currentSelectedPackageButton];
        }
        set
        {
            _structuresList[0].Packages[_currentSelectedPackageButton] = value;
            UpdateCurrentStructure();
        }
    }

    // Аниматор загрузки пакеджа
    private Animator _packageLoadingAnimator;
    // Анимация начала загрузки пакеджа
    private int _packageLoadingAppearAnim = Animator.StringToHash("Appear");
    // Анимация конца загрузки пакеджа
    private int _packageLoadingDisappearAnim = Animator.StringToHash("Disappear");

    // Имя текущего загруженного пакеджа
    private TextMesh _currentPackageNameTextMesh;

    // Табличка блокировки пакеджа
    private GameObject _packageBlocked;

    // Кнопки для выбора пакеджей
    private PackageButton[] _packageButtons;

    // Список структур
    private List<Structure> _structuresList;

    // Выбранная кнопка
    private int _currentSelectedPackageButton = -1;
    // Предыдущая выбранная кнопка
    private int _prevSelectedPackageButton = -1;

    // Флаг: сцена загружается
    private bool _isLoadingScene = false;

    void Awake()
    {
        Instance = this;

        _packageButtons = new PackageButton[14];
        _structuresList = new List<Structure>();

        Transform tmp;

        tmp = transform.FindChild("LevelChange");
        if (tmp != null)
            _packageLoadingAnimator = tmp.GetComponent<Animator>();
        else
            Debug.LogException(new System.Exception("Can't find object LevelChange"));

        tmp = transform.FindChild("Info/NameLevel");
        if (tmp != null)
            _currentPackageNameTextMesh = tmp.GetComponent<TextMesh>();
        else
            Debug.LogException(new System.Exception("Can't find object Info/NameLevel"));

        for (int i = 1; i < 19; i++)
        {
            tmp = transform.FindChild("Packages/PackagesBlock/SectorButton" + i.ToString());
            if (tmp != null)
                _packageButtons[i - 1] = tmp.GetComponent<PackageButton>();
          // Паша. Выключил Елс, потому что сыпал ошибки, типа не находит уровней или че то такое
          //  else
          //      Debug.LogException(new System.Exception("Can't find object Packages/PackagesBlock/SectorButton" + i.ToString()));
        }

        tmp = transform.FindChild("BlockedLevels");
        if (tmp != null)
            _packageBlocked = tmp.gameObject;
        else
            Debug.LogException(new System.Exception("Can't find object BlockedLevels"));
    }

    void Start()
    {
        LoadDefaultConfiguration();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.down);
            if (hit)
            {
                if (hit.transform.name.Contains("SectorButton") && !_isLoadingScene)
                {
                    SelectPackageBtn(hit.transform.GetComponent<PackageButton>());
                    StartCoroutine("LoadCurrentSelectedPackage");
                }
            }
        }
    }

    /// <summary>
    /// Загрузка дефолтной информации о структурах и пакеджах
    /// </summary>
    private void LoadDefaultConfiguration()
    {
        TextAsset structs = Resources.Load("Structures") as TextAsset;
        StructuresConfiguration StructsConfig = JsonConvert.DeserializeObject<StructuresConfiguration>(structs.text);
        _structuresList = StructsConfig.Structures;
        ApplyStructure();
    }

    /// <summary>
    /// Применить структуру
    /// т.е. заполнить кнопки информацией из структуры
    /// </summary>
    private void ApplyStructure()
    {
        // Назначение новых данных всем кнопкам
        for (int i = 0; i < _packageButtons.Length; i++)
        {
            _packageButtons[i].Info = _structuresList[0].Packages[i];
        }
        // Установка первого пакеджа - выбранным
        SelectPackageBtn(_packageButtons[0]);
        // Загрузка первого пакеджа
        StartCoroutine("LoadCurrentSelectedPackage");
    }

    /// <summary>
    /// Обновить информацию о структуре
    /// </summary>
    private void UpdateCurrentStructure()
    {
        // Перебор всех пакетов
        for (int i = 0; i < _structuresList[0].Packages.Count; i++)
        {
            // Если он не доступен
            if (_structuresList[0].Packages[i].State == PACKAGE_STATE.UNAVAIBLE)
            {
                // то проверить, нужно ли его открыть
                int canUnlock = 0;
                foreach (int item in _structuresList[0].Packages[i].PacksNeedForUnlock)
                {
                    if (_structuresList[0].Packages[item].State == PACKAGE_STATE.COMPLITE)
                        canUnlock++;
                }

                // Если необходимый пакет пройден, то canUnlock инкрементируется
                // И если canUnlock равен кол-ву необходимых пакетов, то он открывается
                if (canUnlock == _structuresList[0].Packages[i].PacksNeedForUnlock.Length)
                {
                    _structuresList[0].Packages[i].State = PACKAGE_STATE.AVAIBLE;
                    _packageButtons[i].Info = _structuresList[0].Packages[i];
                }
            }
        }
    }

    /// <summary>
    /// Обработка клика по кнопкам пакеджей
    /// </summary>
    /// <param name="button">Кликнутая кнопка</param>
    private void SelectPackageBtn(PackageButton button)
    {
        // Перебор кнопок
        for (int i = 0; i < _packageButtons.Length; i++)
        {
            // Если это та кнопка по которой кликнули, то
            if (_packageButtons[i].Info.Number == button.Info.Number)
            {
                // если она не является выбранно
                if (i != _currentSelectedPackageButton)
                {
                    // и если она не -1 (это возможно при старте работы)
                    if (_currentSelectedPackageButton != -1)
                        // снять выделение
                        _packageButtons[_currentSelectedPackageButton].Selected = false;

                    _packageButtons[i].Selected = true;
                    _prevSelectedPackageButton = _currentSelectedPackageButton;
                    _currentSelectedPackageButton = i;
                    _currentPackageNameTextMesh.text = "../" + _structuresList[0].Name + "/Package" + _packageButtons[i].Info.Number;
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Корутина загрузки сцены пакеджа
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadCurrentSelectedPackage()
    {
        _packageLoadingAnimator.SetBool("Off", false);
        _isLoadingScene = true;
        _packageBlocked.SetActive(false);
        //_packageLoadingAnimator.Play(_packageLoadingAppearAnim);
        

        if (_prevSelectedPackageButton != -1)
        {
            // -1 возможен при старте работы
            // выгружаем предыдущую сцену, если она есть
            Scene scene = SceneManager.GetSceneByName("Scenes/" + _structuresList[0].Name + "/Package" + _packageButtons[_prevSelectedPackageButton].Info.Number);
            if (scene != null)
                SceneManager.UnloadScene(scene.buildIndex);
        }
        // Асинхронная подгрузка новой сцены
        AsyncOperation async = SceneManager.LoadSceneAsync("Scenes/" + _structuresList[0].Name + "/Package" + _packageButtons[_currentSelectedPackageButton].Info.Number, LoadSceneMode.Additive);
        yield return async;
        _isLoadingScene = false;

        // если загруженый пакедж заблокирован, то отобразить табличку блокировки
        if (_structuresList[0].Packages[_currentSelectedPackageButton].State == PACKAGE_STATE.UNAVAIBLE)
            _packageBlocked.SetActive(true);

        //_packageLoadingAnimator.Play(_packageLoadingDisappearAnim);
        _packageLoadingAnimator.SetBool("Off", true);
    }
}

public class StructuresConfiguration
{
    public List<Structure> Structures { get; set; }
}

public class Structure
{
    public string Name { get; set; }
    public List<PackageInfo> Packages { get; set; }
}

public class PackageInfo
{
    public PackageInfo()
    {
        Number = 0;
        PacksNeedForUnlock = new int[1] { 0 };
        State = PACKAGE_STATE.AVAIBLE;
        Levels = new LevelInfo[4]
        {
            new LevelInfo()
            {
                Number = 1,
                IsComplite = false,
                CompliteTime = 0,
                IsCoinsAchivementUnlocked = false,
                IsDeadAchivementUnlocked = false,
                IsTimeAchivementUnlocked = false
            },
            new LevelInfo()
            {
                Number = 2,
                IsComplite = false,
                CompliteTime = 0,
                IsCoinsAchivementUnlocked = false,
                IsDeadAchivementUnlocked = false,
                IsTimeAchivementUnlocked = false
            },
            new LevelInfo()
            {
                Number = 3,
                IsComplite = false,
                CompliteTime = 0,
                IsCoinsAchivementUnlocked = false,
                IsDeadAchivementUnlocked = false,
                IsTimeAchivementUnlocked = false
            },
            new LevelInfo()
            {
                Number = 4,
                IsComplite = false,
                CompliteTime = 0,
                IsCoinsAchivementUnlocked = false,
                IsDeadAchivementUnlocked = false,
                IsTimeAchivementUnlocked = false
            }
        };
        Time = 90;
    }
    public int Number { get; set; }
    public int[] PacksNeedForUnlock { get; set; }
    public PACKAGE_STATE State { get; set; }
    public LevelInfo[] Levels { get; set; }
    public int Time { get; set; }
}

public enum PACKAGE_STATE
{
    AVAIBLE,
    UNAVAIBLE,
    COMPLITE,
    LOCKED
}

public class LevelInfo
{
    public int Number { get; set; }
    public bool IsComplite { get; set; }
    public bool IsCoinsAchivementUnlocked { get; set; }
    public bool IsTimeAchivementUnlocked { get; set; }
    public bool IsDeadAchivementUnlocked { get; set; }
    public float CompliteTime { get; set; }
}
