﻿using UnityEngine;
using System.Collections;

public class TimeLine : MonoBehaviour
{
    /// <summary>
    /// Максимальное время шкалы
    /// </summary>
    public float MaxTime = 180f;

    /// <summary>
    /// Значение для единицы времени, которые собирает игрок
    /// </summary>
    public float TimeCoinValue = 1f;

    /// <summary>
    /// Инстанс
    /// </summary>
    public static TimeLine Instance;

    // Таймер
    private TextMesh _timerText;

    // Полоса таймер
    private Transform _timerLine;

    // Шаг линии
    private float _lineStep;

    // Значение таймера
    private float _timerValue;

    // Предыдущая остановка таймера
    private float _prevTimeStamp;

    // Флаг: остановлен ли таймер?
    private bool _isStoped = true;

    void Awake()
    {
        Instance = this;
        Transform tmp;

        tmp = transform.FindChild("TXT-Time");
        if (tmp == null)
            Debug.LogException(new System.Exception("GamePlayElements didn't find in " + transform.name));
        else
            _timerText = tmp.GetComponent<TextMesh>();

        _timerLine = transform.FindChild("ControlsLine/TimeLine-B");
        if (_timerLine == null)
            Debug.LogException(new System.Exception("TimeLine-B didn't find in " + transform.name));

        _lineStep = 1f / MaxTime;
    }

    void Update()
    {
        if (!_isStoped)
            _timerValue -= Time.deltaTime;

        float pos = (_timerValue * _lineStep) * 10;
        _timerLine.localPosition = new Vector3(pos, 0, 0);

        _timerText.text = _timerValue.ToString();
    }

    /// <summary>
    /// Установка значения таймера
    /// </summary>
    /// <param name="value"></param>
    public void SetValue(float value)
    {
        _timerValue = value;
        _prevTimeStamp = value;
    }

    /// <summary>
    /// Старт таймера c предыдущего останова
    /// </summary>
    public void StartTimer()
    {
        _timerValue = _prevTimeStamp;
        _isStoped = false;
    }

    /// <summary>
    /// Продолжить отсчет времени с текущей позиции
    /// </summary>
    public void Resume()
    {
        _isStoped = false;
    }

    /// <summary>
    /// Остановить таймер
    /// </summary>
    public void Stop()
    {
        _prevTimeStamp = _timerValue;
        _isStoped = true;
    }

    /// <summary>
    /// Увеличить значение таймера на единицу времени
    /// </summary>
    public void IncreaseTime()
    {
        _timerValue += TimeCoinValue;
    }

    /// <summary>
    /// Событие конца времени
    /// </summary>
    public event System.EventHandler<System.EventArgs> TimeOver;
    private void OnTimeOver()
    {
        System.EventHandler<System.EventArgs> handler = TimeOver;
        if (handler != null)
            handler(this, new System.EventArgs());
    }
}
